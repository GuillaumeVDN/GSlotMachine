package com.guillaumevdn.gslotmachine;

import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.string.TextFile;
import com.guillaumevdn.gcore.libs.com.google.gson.GsonBuilder;
import com.guillaumevdn.gslotmachine.command.CmdEdit;
import com.guillaumevdn.gslotmachine.command.machine.CmdCreateMachine;
import com.guillaumevdn.gslotmachine.command.machine.CmdDeleteMachine;
import com.guillaumevdn.gslotmachine.command.machine.CmdEditMachine;
import com.guillaumevdn.gslotmachine.command.machine.CmdListMachines;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.serialization.SerializerGSM;
import com.guillaumevdn.gslotmachine.lib.serialization.adapter.AdapterMachine;
import com.guillaumevdn.gslotmachine.listeners.PlayerEvents;
import com.guillaumevdn.gslotmachine.migration.v3_0.MigrationV3Config;
import com.guillaumevdn.gslotmachine.migration.v3_0.MigrationV3Data;

/**
 * @author GuillaumeVDN
 */
public final class GSlotMachine extends GPlugin<ConfigGSM, PermissionGSM> {

	private static GSlotMachine instance;
	public static GSlotMachine inst() { return instance; }

	public GSlotMachine() {
		super(55107, "gslotmachine", "machine", ConfigGSM.class, PermissionGSM.class, "data_v3",
				MigrationV3Config.class, MigrationV3Data.class
				);
		instance = this;
	}

	@Override
	public GsonBuilder createGsonBuilder() {
		return super.createGsonBuilder().registerTypeAdapter(Machine.class, AdapterMachine.INSTANCE.getGsonAdapter());
	}

	// ----- plugin

	@Override
	protected void registerTypes() {
		SerializerGSM.init();
	}

	@Override
	protected void registerTexts() {
		registerTextFile(new TextFile<>(GSlotMachine.inst(), "gslotmachine.yml", TextGSM.class));
		registerTextFile(new TextFile<>(GSlotMachine.inst(), "gslotmachine_editor.yml", TextEditorGSM.class));
	}

	@Override
	protected void registerData() {
		registerDataBoard(new BoardMachines());
	}

	@Override
	protected void enable() throws Throwable {
		getMainCommand().setSubcommand(new CmdEdit());
		getMainCommand().setSubcommand(new CmdCreateMachine());
		getMainCommand().setSubcommand(new CmdEditMachine());
		getMainCommand().setSubcommand(new CmdDeleteMachine());
		getMainCommand().setSubcommand(new CmdListMachines());

		registerListener("player", new PlayerEvents());
	}

}
