package com.guillaumevdn.gslotmachine.data.machine;

import java.io.File;

import com.guillaumevdn.gcore.GCore;
import com.guillaumevdn.gcore.lib.bukkit.BukkitThread;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyed;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyedJson;
import com.guillaumevdn.gcore.lib.data.board.keyed.ConnectorKeyedSQL;
import com.guillaumevdn.gcore.lib.data.board.keyed.KeyedBoardLocal;
import com.guillaumevdn.gcore.lib.data.sql.SQLHandler;
import com.guillaumevdn.gcore.lib.data.sql.SQLiteHandler;
import com.guillaumevdn.gcore.lib.file.FileUtils;
import com.guillaumevdn.gslotmachine.GSlotMachine;

/**
 * @author GuillaumeVDN
 */
public class BoardMachines extends KeyedBoardLocal<String, Machine> {

	private static BoardMachines instance = null;
	public static BoardMachines inst() { return instance; }

	public BoardMachines() {
		super(GSlotMachine.inst(), "gslotmachine_machines_v3", Machine.class, 20 * 60);
		instance = this;
	}

	// ----------------------------------------------------------------------------------------------------
	// data
	// ----------------------------------------------------------------------------------------------------

	@Override
	protected void beforeDisposeCacheElement(BukkitThread thread, String key, Machine machine) {
		if (machine != null) {
			if (machine.getActive() != null) {
				machine.getActive().stop(true);
			}
		}
	}

	// ----------------------------------------------------------------------------------------------------
	// json
	// ----------------------------------------------------------------------------------------------------

	@Override
	protected ConnectorKeyed<String, Machine> createConnectorJson() {
		return new ConnectorKeyedJson<String, Machine>(this) {
			@Override
			public File getRoot() {
				return GSlotMachine.inst().getDataFile("data_v3/machines/");
			}

			@Override
			public File getFile(String key) {
				return GSlotMachine.inst().getDataFile("data_v3/machines/" + key + ".json");
			}

			@Override
			public String getKey(File file) {
				return FileUtils.getSimpleName(file);
			}
		};
	}

	// ----------------------------------------------------------------------------------------------------
	// mysql
	// ----------------------------------------------------------------------------------------------------

	private ConnectorKeyedSQL<String, Machine> createConnectorSQL(SQLHandler handler) {
		return new ConnectorKeyedSQL<String, Machine>(this, handler) {
			@Override
			public String keyName() {
				return "machine_id";
			}
			
			@Override
			public String keyType() {
				return "VARCHAR(50)";
			}

			@Override
			protected String decodeKey(String raw) {
				return raw;
			}

			@Override
			protected Machine decodeValue(String jsonData) {
				return GSlotMachine.inst().getGson().fromJson(jsonData, Machine.class);
			}

			@Override
			protected String encodeValue(Machine value) {
				return GSlotMachine.inst().getGson().toJson(value);
			}
		};
	}

	@Override
	protected ConnectorKeyed<String, Machine> createConnectorMySQL() {
		return createConnectorSQL(GCore.inst().getMySQLHandler());
	}

	@Override
	protected ConnectorKeyed<String, Machine> createConnectorSQLite() {
		return createConnectorSQL(new SQLiteHandler(GSlotMachine.inst().getDataFile("data_v3/machines.sqlite.db")));
	}

}
