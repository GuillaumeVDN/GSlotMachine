package com.guillaumevdn.gslotmachine;

import com.guillaumevdn.gcore.lib.string.TextElement;
import com.guillaumevdn.gcore.lib.string.TextEnumElement;

/**
 * @author GuillaumeVDN
 */
public enum TextEditorGSM implements TextEnumElement {

	// ----- machine

	descriptionMachineCost,
	descriptionMachineCostCurrency,
	descriptionMachineAnimationSound,
	descriptionMachineRefundOnDisconnect,
	descriptionMachinePrizes,
	
	descriptionMachinePrizeChance,
	descriptionMachinePrizeDisplayType,
	descriptionMachinePrizeGiveItems,
	descriptionMachinePrizeExecuteCommands,
	descriptionMachinePrizeDescription

	;

	private TextElement text = new TextElement();

	TextEditorGSM() {
	}

	@Override
	public String getId() {
		return name();
	}

	@Override
	public TextElement getText() {
		return text;
	}

}
