package com.guillaumevdn.gslotmachine.lib.machine.element;

import java.io.File;

import com.guillaumevdn.gcore.lib.element.struct.list.referenceable.ElementsContainer;
import com.guillaumevdn.gslotmachine.GSlotMachine;

/**
 * @author GuillaumeVDN
 */
public class MachineTypesContainer extends ElementsContainer<ElementMachineType> {

	public MachineTypesContainer() {
		super(GSlotMachine.inst(), "machine type", ElementMachineType.class, GSlotMachine.inst().getDataFile("machine_types/"));
	}

	@Override
	protected ElementMachineType createElement(File file, String id) {
		return new ElementMachineType(file, id);
	}

}
