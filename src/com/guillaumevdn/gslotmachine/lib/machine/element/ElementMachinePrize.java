package com.guillaumevdn.gslotmachine.lib.machine.element;

import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.element.struct.Element;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.container.ContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementChancePercentage;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementMat;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementStringList;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementText;
import com.guillaumevdn.gcore.lib.element.type.container.ElementItemMode;
import com.guillaumevdn.gcore.lib.element.type.list.ElementItemList;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.TextEditorGSM;

/**
 * @author GuillaumeVDN
 */
public class ElementMachinePrize extends ContainerElement {

	private ElementChancePercentage chance = addChancePercentage("chance", Need.required(), TextEditorGSM.descriptionMachinePrizeChance);
	private ElementMat displayType = addMat("display_type", Need.required(), TextEditorGSM.descriptionMachinePrizeDisplayType);
	private ElementItemList giveItems = addItemList("give_items", Need.optional(), ElementItemMode.BUILDABLE, TextEditorGSM.descriptionMachinePrizeGiveItems);
	private ElementStringList executeCommands = addStringList("execute_commands", Need.optional(), TextEditorGSM.descriptionMachinePrizeExecuteCommands);
	private ElementText description = addText("description", Need.required(), TextEditorGSM.descriptionMachinePrizeDescription);

	public ElementMachinePrize(Element parent, String id, Need need, Text editorDescription) {
		super(parent, id, need, editorDescription);
	}

	public ElementChancePercentage getChance() {
		return chance;
	}

	public ElementMat getDisplayType() {
		return displayType;
	}

	public ElementItemList getGiveItems() {
		return giveItems;
	}

	public ElementStringList getExecuteCommands() {
		return executeCommands;
	}

	public ElementText getDescription() {
		return description;
	}

	// ----- editor

	@Override
	public Mat editorIconType() {
		return CommonMats.GOLD_INGOT;
	}

}
