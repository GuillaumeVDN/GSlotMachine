package com.guillaumevdn.gslotmachine.lib.machine.element;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.guillaumevdn.gcore.lib.GPlugin;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.element.struct.Need;
import com.guillaumevdn.gcore.lib.element.struct.SuperElement;
import com.guillaumevdn.gcore.lib.element.struct.container.ContainerElement;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementBoolean;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementCurrency;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementDouble;
import com.guillaumevdn.gcore.lib.element.type.basic.ElementSound;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.TextEditorGSM;

/**
 * @author GuillaumeVDN
 */
public class ElementMachineType extends ContainerElement implements SuperElement {

	private ElementDouble cost = addDouble("cost", Need.optional(), 0d, TextEditorGSM.descriptionMachineCost);
	private ElementCurrency costCurrency = addCurrency("cost_currency", Need.optional(), TextEditorGSM.descriptionMachineCostCurrency);
	private ElementBoolean refundOnDisconnect = addBoolean("refund_on_disconnect", Need.optional(false), TextEditorGSM.descriptionMachineRefundOnDisconnect);
	private ElementSound animationSound = addSound("animation_sound", Need.optional(), TextEditorGSM.descriptionMachineAnimationSound);
	private ElementMachinePrizeList prizes = add(new ElementMachinePrizeList(this, "prizes", Need.required(), TextEditorGSM.descriptionMachinePrizes));

	public ElementMachineType(File file, String id) {
		super(null, id, Need.optional(), null);
		this.file = file;
	}

	public ElementDouble getCost() {
		return cost;
	}

	public ElementCurrency getCostCurrency() {
		return costCurrency;
	}

	public ElementBoolean getRefundOnDisconnect() {
		return refundOnDisconnect;
	}

	public ElementSound getAnimationSound() {
		return animationSound;
	}

	public ElementMachinePrizeList getPrizes() {
		return prizes;
	}

	// ----- editor

	@Override
	public Mat editorIconType() {
		return CommonMats.GOLD_INGOT;
	}

	// ----- super element

	private File file;
	private List<String> loadErrors = new ArrayList<>();
	protected YMLConfiguration config = null;

	@Override public GPlugin getPlugin() { return GSlotMachine.inst(); }
	@Override public File getOwnFile() { return file; }
	@Override public List<String> getLoadErrors() { return Collections.unmodifiableList(loadErrors); }
	@Override public YMLConfiguration getConfiguration() { if (config == null) { reloadConfiguration(); } return config; }
	@Override public String getConfigurationPath() { return ""; }
	@Override public void addLoadError(String error) { loadErrors.add(error); }
	@Override public void reloadConfiguration() { this.config = new YMLConfiguration(getPlugin(), file); }

}
