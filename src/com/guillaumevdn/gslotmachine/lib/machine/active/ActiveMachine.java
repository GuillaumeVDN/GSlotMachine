package com.guillaumevdn.gslotmachine.lib.machine.active;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.compatibility.material.CommonMats;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.compatibility.sound.Sound;
import com.guillaumevdn.gcore.lib.item.ItemUtils;
import com.guillaumevdn.gcore.lib.number.NumberUtils;
import com.guillaumevdn.gcore.lib.player.PlayerUtils;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gcore.lib.tuple.Triple;
import com.guillaumevdn.gcore.lib.wrapper.WrapperInteger;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.machine.element.ElementMachinePrize;

/**
 * @author GuillaumeVDN
 */
public class ActiveMachine {

	private final Machine machine;
	private final OfflinePlayer player;
	private final Replacer replacer;
	private final String taskId;

	private final Map<Integer, Item> currentItems = new HashMap<>();
	private final Map<Integer, Mat> ogCases = new HashMap<>();
	private ElementMachinePrize result;

	public ActiveMachine(Machine machine, OfflinePlayer player) {
		this.machine = machine;
		this.player = player;
		final Player online = player.getPlayer();
		this.replacer = Replacer
				.ofPlayer(() -> player.getPlayer())
				.with("{player}", () -> player.getName())
				.with("{player_x}", () -> online.getLocation().getX())
				.with("{player_y}", () -> online.getLocation().getY())
				.with("{player_z}", () -> online.getLocation().getZ())
				;
		this.taskId = "machine_" + machine.getId();
	}

	public void start() {
		// precalculate prize
		final List<Triple<Double, Double, ElementMachinePrize>> parsed = new ArrayList<>();
		double lastMin = 0d;
		for (ElementMachinePrize prize : machine.getTypeElement().getPrizes().values()) {
			final double chance = prize.getChance().parseGeneric().orElse(0d);
			parsed.add(Triple.of(lastMin, lastMin + chance, prize));
			lastMin = lastMin + chance;
		}

		final double chance = NumberUtils.random(0d, lastMin < 100d ? 100d : lastMin);  // can lose if total is less than 100
		final Triple<Double, Double, ElementMachinePrize> found = parsed.stream().filter(p -> chance >= p.getA() && chance < p.getB()).findFirst().orElse(null);
		result = found != null ? found.getC() : null;

		// precalculate tend to
		final Map<Integer, ElementMachinePrize> tendTo = new HashMap<>();

		if (result != null) {
			machine.getCases().forEach((nb, loc) -> {
				tendTo.put(nb, result);
			});
		} else {
			int tries = 0;
			do {
				machine.getCases().forEach((nb, loc) -> {
					tendTo.put(nb, CollectionUtils.random(machine.getTypeElement().getPrizes().values()));
				});
			} while (++tries <= 25 && CollectionUtils.allEqual(tendTo.values()));
		}

		// open cases
		machine.getCases().forEach((nb, loc) -> {
			final Block block = loc.getBlock();
			ogCases.put(nb, Mat.fromBlock(block).orAir());
			CommonMats.AIR.setBlock(block);
		});

		// start running
		final int caseFixTime = Math.floorDiv(ConfigGSM.totalDurationTicks - ConfigGSM.finalFreezeDurationTicks, machine.getCases().size());
		final Sound animationSound = machine.getTypeElement().getAnimationSound().parse(replacer).orNull();

		final WrapperInteger elapsedTicks = WrapperInteger.of(0);
		final WrapperInteger remainingDelayTicks = WrapperInteger.of(ConfigGSM.initialDelayTicks);

		GSlotMachine.inst().registerTask(taskId, false, 1, () -> {
			// end
			if (elapsedTicks.alter(1) >= ConfigGSM.totalDurationTicks) {
				stop(false);
				return;
			}

			// animate and update delay
			if (remainingDelayTicks.alter(-1) <= 0) {
				// set new items
				machine.getCases().forEach((nb, loc) -> {
					final int fixTime = caseFixTime * nb;
					final ElementMachinePrize prize = elapsedTicks.get() >= fixTime ? tendTo.get(nb) : CollectionUtils.random(machine.getTypeElement().getPrizes().values());
					setCase(nb, prize);
				});

				// sound
				if (animationSound != null && player.isOnline()) {
					animationSound.play(player.getPlayer());
				}

				// change delay
				for (int afterTicks : ConfigGSM.changeDelays.keySet()) {  // high to low
					if (elapsedTicks.get() >= afterTicks) {
						remainingDelayTicks.set(ConfigGSM.changeDelays.get(afterTicks));
						break;
					}
				}
			}
		});
	}

	private void setCase(int nb, ElementMachinePrize prize) {
		if (prize == null) {
			return;  // might happen if no prize set in config
		}

		final Location loc = machine.getCase(nb);
		final ItemStack stack = prize.getDisplayType().parse(replacer).filter(m -> m.canHaveItem()).ifPresentMap(m -> m.newStack()).orNull();

		if (loc == null || stack == null) {
			return;
		}

		// spawn or replace
		Item item = currentItems.get(nb);
		if (item == null) {
			item = loc.getWorld().dropItem(loc, stack);
			item.setPickupDelay(Integer.MAX_VALUE);
			item.setVelocity(new Vector(0, 0, 0));
			currentItems.put(nb, item);
		} else {
			item.setItemStack(stack);
		}

		// send particles
		final List<Player> players = loc.getWorld().getPlayers();
		ConfigGSM.caseParticles.forEach(particle -> {
			particle.send(players, loc);
		});
	}

	public void stop(boolean cancel) {
		GSlotMachine.inst().stopTask(taskId);

		// give prize and notify
		final Player playerOnline = player.getPlayer();
		if (!cancel && playerOnline != null) {

			if (result != null) {
				result.getGiveItems().parse(replacer).ifPresentForEach((ItemStack it) -> {
					ItemUtils.give(playerOnline, it, false);
				});
				playerOnline.updateInventory();
				result.getExecuteCommands().parse(replacer).ifPresentForEach((String cmd) -> {
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), cmd);  // {player} already replaced by replacer
				});

				final Replacer rep = replacer.cloneReplacer().with("{prize}", () -> result.getDescription().parse(replacer).orElse(Text.of("")).getCurrentLines());
				ConfigGSM.notifyWin.sendAll(playerOnline, rep);
				ConfigGSM.notifyWinBroadcast.sendAll(PlayerUtils.getOnline(), rep);
			} else {
				ConfigGSM.notifyLose.sendAll(playerOnline, replacer);
			}
		}

		// remove items
		currentItems.values().forEach(it -> it.remove());

		// reset cases
		ogCases.forEach((nb, type) -> {
			final Location loc = machine.getCases().get(nb);
			if (loc != null) {
				type.setBlock(loc.getBlock());
			}
		});

		// reset active machine
		machine.setActive(null);
	}

}
