package com.guillaumevdn.gslotmachine.lib.command;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.argument.PartialArgument;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;

/**
 * @author GuillaumeVDN
 */
public class ArgumentMachine extends PartialArgument<Machine> {

	public ArgumentMachine(NeedType need, boolean playerOnly, Permission permission, Text usage) {
		super(need, playerOnly, permission, usage);
	}

	@Override
	protected Machine exactMatch(CommandCall call, String arg) {
		return BoardMachines.inst().getCachedValue(arg);
	}

	@Override
	protected Stream<Machine> partialMatches(CommandCall call, String arg) {
		return BoardMachines.inst().copyCacheValues().stream().filter(arena -> arena.getId().toLowerCase().startsWith(arg));
	}

	@Override
	public List<String> tabComplete(CommandCall call) {
		return BoardMachines.inst() == null ? new ArrayList<>(0) : BoardMachines.inst().streamResultValues(str -> str.map(Machine::getId).collect(Collectors.toList()));
	}

}
