package com.guillaumevdn.gslotmachine.lib.serialization;

import com.guillaumevdn.gcore.lib.serialization.Serializer;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.machine.element.ElementMachineType;

/**
 * @author GuillaumeVDN
 */
public final class SerializerGSM {

	public static final Serializer<Machine> MACHINE = Serializer.of(Machine.class, arena -> arena.getId(), id -> Machine.cachedOrNull(id));
	public static final Serializer<ElementMachineType> MACHINE_TYPE = Serializer.ofContainer(ElementMachineType.class, () -> ConfigGSM.machineTypes);

	public static void init() {}

}
