package com.guillaumevdn.gslotmachine;

import com.guillaumevdn.gcore.lib.permission.Permission;
import com.guillaumevdn.gcore.lib.permission.PermissionContainer;

/**
 * @author GuillaumeVDN
 */
public class PermissionGSM extends PermissionContainer {

	private static PermissionGSM instance = null;
	public static PermissionGSM inst() { return instance; }

	public PermissionGSM() {
		super(GSlotMachine.inst());
		instance = this;
	}

	public final Permission gslotmachineAdmin = setAdmin("gslotmachine.admin");
	public final Permission gslotmachineEdit = set("gslotmachine.edit");

}
