package com.guillaumevdn.gslotmachine.migration.v3_0;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcore.lib.configuration.file.YMLError;
import com.guillaumevdn.gcore.lib.file.FileUtils;
import com.guillaumevdn.gcore.lib.file.ResourceExtractor;
import com.guillaumevdn.gcore.lib.migration.BackupBehavior;
import com.guillaumevdn.gcore.lib.migration.Migration;
import com.guillaumevdn.gcore.lib.migration.SilentFail;
import com.guillaumevdn.gcore.lib.object.ObjectUtils;
import com.guillaumevdn.gcore.migration.YMLMigrationReading;
import com.guillaumevdn.gcore.migration.v8_0.config.MigrationV8Config;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.lib.serialization.SerializerGSM;

/**
 * @author GuillaumeVDN
 */
public final class MigrationV3Config extends Migration {

	public MigrationV3Config() {
		super(GSlotMachine.inst(), "v2", "v2 -> v3 (config)", "data_v3/migrated_v3.0.0_config.DONTREMOVE");
	}

	@Override
	public boolean mustMigrate() {
		return super.mustMigrate() && getPluginFile("texts.yml").exists();
	}

	@Override
	protected void doMigrate() throws Throwable {
		// init serializers
		SerializerGSM.init();

		// make sure all .yml files can be loaded with our new replacer
		List<String> errors = new ArrayList<>();
		attemptFilesOperation("making sure all files can be loaded by the new YAML parser", "file", BackupBehavior.RESTORE, getPluginFolder(), filterYMLIfNotOneOf("texts.yml"),
				file -> {
					try {
						new YMLConfiguration(getPlugin(), file);
					} catch (Throwable cause) {
						YMLError causeYML = ObjectUtils.findCauseOrNull(cause, YMLError.class);
						YMLError causeConfig = ObjectUtils.findCauseOrNull(cause, YMLError.class);
						List<String> errs = CollectionUtils.asList("This file can't be loaded with the new YAML parser : " + file.getPath());
						if (causeYML != null || causeConfig != null) {
							errs.add((causeYML != null ? causeYML : causeConfig).getMessage());
							errors.addAll(errs);
						} else {
							fail(errs, cause, BackupBehavior.RESTORE);
							throw new SilentFail();
						}
					}
				});

		attemptOperation("making sure all files can be loaded by the new YAML parser", BackupBehavior.RESTORE, () -> {
			if (!errors.isEmpty()) {
				fail(errors, null, BackupBehavior.RESTORE);
				throw new SilentFail();
			}
		});

		// extract new default configs
		attemptOperation("extracting default configs", BackupBehavior.RESTORE, () -> {
			if (!FileUtils.delete(getPluginFolder())) {
				fail("couldn't delete current folder", null, BackupBehavior.RESTORE);
				throw new SilentFail();
			}
			new ResourceExtractor(getPlugin(), getPluginFolder(), "resources/").extract(false, true);
		});

		// config
		File oldConfigFile = new File(getBackupFolder() + "/config.yml");
		attemptOperation("converting config.yml", BackupBehavior.RESTORE, () -> {
			if (oldConfigFile.exists()) {
				YMLMigrationReading oldConfig = new YMLMigrationReading(getPlugin(), oldConfigFile);
				YMLConfiguration config = new YMLConfiguration(getPlugin(), new File(getPluginFolder() + "/config.yml"));

				// data
				if (oldConfig.readString("data.backend", "").equalsIgnoreCase("MYSQL")) {
					config.write("data_backend.gslotmachine_machines_v3", "MYSQL");
				}

				// save file
				countMod();
				config.save();
			}
		});

		// migration
		attemptSingleYMLFileToMultiple("converting machines", "machine", BackupBehavior.RESTORE, oldConfigFile, "types", id -> getPluginFile("machine_types/" + id + ".yml"), (src, o, target) -> {

			target.write("cost", src.readDouble(o + ".cost", 0d));
			target.write("cost_currency", "VAULT");
			target.write("animation_sound", src.readString(o + ".animation_sound", "BLOCK_WOODEN_BUTTON_CLICK_ON"));
			target.write("refund_on_disconnect", src.readBoolean(o + ".refund_on_disconnect", true));

			for (final String prizeId : src.readKeysForSection(o + ".prizes")) {
				final String op = o + ".prizes." + prizeId;
				final String n = "prizes." + prizeId;

				final String type = src.readString(op + ".type", "COBBLESTONE");
				final int amount = src.readInteger(op + ".amont", 1);

				target.write(n + ".chance", src.readDouble(op + ".chance", 10d));
				target.write(n + ".display_type", type);

				if (src.readBoolean(op + ".give_item", true)) {
					MigrationV8Config.migrateItem(this, src, op, target, n + ".give_items.1", true);
				}

				target.write(n + ".execute_commands", src.readStringList(op + ".commands", null));
				target.write(n + ".description", type + " x" + amount);
			}

			// count mod, this will save the file
			countMod();
		});

		// data is another migration, so we don't have to remigrate files if it fails
	}

}
