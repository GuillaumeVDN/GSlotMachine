package com.guillaumevdn.gslotmachine.command.machine;

import com.guillaumevdn.gcore.lib.bukkit.BukkitThread;
import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.PermissionGSM;
import com.guillaumevdn.gslotmachine.TextGSM;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.command.ArgumentMachine;

public class CmdDeleteMachine extends Subcommand {

	private final ArgumentMachine argMachine = addArgument(new ArgumentMachine(NeedType.REQUIRED, false, null, Text.of("machine")));

	public CmdDeleteMachine() {
		super(true, PermissionGSM.inst().gslotmachineAdmin, TextGSM.commandDescriptionGslotmachineDeleteMachine, ConfigGSM.commandsAliasesDeleteMachine);
	}

	@Override
	public void perform(CommandCall call) {
		final Machine machine = argMachine.get(call);

		if (machine.getActive() != null) {
			machine.getActive().stop(true);
		}

		BoardMachines.inst().deleteElements(BukkitThread.ASYNC, CollectionUtils.asSet(machine.getId()), () -> {
			Text.of("&7Machine &e" + machine.getId() + " &7was deleted.").send(call);
		});
	}

}
