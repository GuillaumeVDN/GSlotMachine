package com.guillaumevdn.gslotmachine.command.machine;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.PermissionGSM;
import com.guillaumevdn.gslotmachine.TextGSM;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;

public class CmdListMachines extends Subcommand {

	public CmdListMachines() {
		super(false, PermissionGSM.inst().gslotmachineAdmin, TextGSM.commandDescriptionGslotmachineListMachines, ConfigGSM.commandsAliasesListMachines);
	}

	@Override
	public void perform(CommandCall call) {
		final List<Machine> machines = BoardMachines.inst().copyCacheValues();
		final Map<Machine, Integer> distanceToPlayer = new HashMap<>();

		final Player player = call.getSenderPlayer();
		if (player != null) {
			final Location loc = player.getLocation();
			machines.forEach(machine -> {
				if (machine.getButton() != null && loc.getWorld().equals(machine.getButton().getWorld())) {
					distanceToPlayer.put(machine, (int) loc.distance(machine.getButton()));
				}
			});
			machines.sort(Comparator.comparing(m -> distanceToPlayer.get(m)));
		} else {
			machines.sort(Comparator.comparing(m -> m.getId()));
		}

		if (machines.isEmpty()) {
			Text.of("&7There is no machine.");
		} else {
			Text.of("&7" + (machines.size() == 1 ? "There is 1 machine" : "There are " + machines.size() + " machines") + " :");

			machines.forEach(machine -> {
				final Integer distance = distanceToPlayer.get(machine);

				Text.of("&7- &e" + machine.getId()
				+ "&7: " + (machine.isComplete() ? "&acomplete" : "&cnot complete")
				+ (distance == null ? "&7 (in world " + machine.getButton().getWorld().getName() + ")" : "&7 (" + distance + "m)"))
				.send(call);
			});
		}

	}

}
