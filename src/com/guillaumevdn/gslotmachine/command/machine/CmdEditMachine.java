package com.guillaumevdn.gslotmachine.command.machine;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcore.lib.collection.CollectionUtils;
import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentFixed;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentPairFixedStringDynamicFakeEnum;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentPairFixedStringInteger;
import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.serialization.Serializer;
import com.guillaumevdn.gcore.lib.string.StringUtils;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.PermissionGSM;
import com.guillaumevdn.gslotmachine.TextGSM;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.command.ArgumentMachine;
import com.guillaumevdn.gslotmachine.lib.machine.element.ElementMachineType;

public class CmdEditMachine extends Subcommand {

	private final ArgumentMachine argMachine = addArgument(new ArgumentMachine(NeedType.REQUIRED, false, null, Text.of("machine")));

	private final ArgumentPairFixedStringDynamicFakeEnum<ElementMachineType> argSetType = addArgument(new ArgumentPairFixedStringDynamicFakeEnum(NeedType.OPTIONAL, false, null, Text.of("settype <type>"), CollectionUtils.asList("settype"), ElementMachineType.class, () -> CollectionUtils.asList(ConfigGSM.machineTypes.values())));
	private final ArgumentFixed argSetButton = addArgumentFixed(NeedType.OPTIONAL, false, null, "setbutton");

	private final ArgumentFixed argCaseList = addArgumentFixed(NeedType.OPTIONAL, false, null, "caselist");

	private final ArgumentPairFixedStringInteger argSetCase = addArgument(new ArgumentPairFixedStringInteger(NeedType.OPTIONAL, false, null, Text.of("setcase 1"), CollectionUtils.asList("setcase")));
	private final ArgumentPairFixedStringInteger argDelCase = addArgument(new ArgumentPairFixedStringInteger(NeedType.OPTIONAL, false, null, Text.of("delcase 1"), CollectionUtils.asList("delcase")));

	public CmdEditMachine() {
		super(true, PermissionGSM.inst().gslotmachineAdmin, TextGSM.commandDescriptionGslotmachineEditMachine, ConfigGSM.commandsAliasesEditMachine);

		setIncompatible(argCaseList, argSetType, argSetButton, argSetCase, argDelCase);
	}

	@Override
	public void perform(CommandCall call) {
		final Machine machine = argMachine.get(call);
		final Player sender = call.getSenderPlayer();

		// set machine type
		final ElementMachineType type = argSetType.get(call);
		if (type != null) {
			machine.setType(type);
			Text.of("&7Set type to &a" + type + "&7 for machine &a" + machine.getId() + "&7.").send(call);
			machine.sendNextStepMessage(sender);
			return;
		}

		// set button location
		if (argSetButton.get(call) != null) {
			final Block button = sender.getTargetBlock(null, 5);
			if (!Mat.fromBlock(button).orAir().getData().getDataName().contains("BUTTON")) {
				Text.of("&7You must be looking at a button block.").send(call);
			} else {
				machine.setButton(button.getLocation());
				Text.of("&7Set button &7for machine &a" + machine.getId() + "&7.").send(call);
				machine.sendNextStepMessage(sender);
			}
			return;
		}

		// list cases
		if (argCaseList.get(call) != null) {
			if (machine.getCases().isEmpty()) {
				Text.of("&7Machine &e" + machine.getId() + "&7 has no case.").send(call);
			} else {
				Text.of("&7Machine &a" + machine.getId() + "&7 has " + StringUtils.pluralizeAmountDesc("case", machine.getCases().size()) + " :").send(call);
				machine.getCases().forEach((id, loc) -> {
					Text.of("&7- &a" + id + "&7 : &a" + Serializer.LOCATION.serialize(loc) + (loc.getWorld().equals(sender.getWorld()) ? "&7 (" + (int) loc.distance(sender.getLocation()) + "m)" : "")).send(call);
				});
			}
			return;
		}

		// set/delete case
		Integer caseNumber = argSetCase.get(call);
		if (caseNumber != null) {
			if (caseNumber <= 0) {
				Text.of("&7Case number must be at least 1.").send(call);
			} else {
				final Block caseBlock = sender.getTargetBlock(null, 5);
				if (Mat.fromBlock(caseBlock).orAir().isAir()) {
					Text.of("&7You must be looking at a block.").send(call);
				} else {
					machine.setCase(caseNumber, caseBlock.getLocation().clone().add(0.5d, 0d, 0.5d));
					Text.of("&7Set case location &a" + caseNumber + "&7 for machine &a" + machine.getId() + "&7.").send(call);
					machine.sendNextStepMessage(sender);
				}
			}
			return;
		}

		caseNumber = argDelCase.get(call);
		if (caseNumber != null) {
			if (machine.getCase(caseNumber) == null) {
				Text.of("&7There's no case location &c" + caseNumber + "&7 for machine &c" + machine.getId() + "&7.").send(call);
			} else {
				machine.removeCase(caseNumber);
				Text.of("&7Removed case location &e" + caseNumber + "&7 for machine &e" + machine.getId() + "&7.").send(call);
				machine.sendNextStepMessage(sender);
			}
			return;
		}

		// current settings
		Text.of("&7Use arguments to edit machine &e" + machine.getId() + "&7. Current settings :").send(call);
		if (machine.getTypeElement() != null) {
			Text.of("&7- Type : &e" + machine.getTypeElement().getId()).send(call);
		}
		if (machine.getButton() != null) {
			Text.of("&7- Button location : &e" + Serializer.LOCATION.serialize(machine.getButton()) + (machine.getButton().getWorld().equals(sender.getWorld()) ? "&7 (" + (int) machine.getButton().distance(sender.getLocation()) + "m)" : "")).send(call);
		}
		if (!machine.getCases().isEmpty()) {
			Text.of("&7- Case" + StringUtils.pluralize(machine.getCases().size()) + " :").send(call);
			machine.getCases().forEach((id, loc) -> {
				Text.of("&7  > &a" + id + "&7 : &a" + Serializer.LOCATION.serialize(loc) + (loc.getWorld().equals(sender.getWorld()) ? "&7 (" + (int) loc.distance(sender.getLocation()) + "m)" : "")).send(call);
			});
		}
		Text.of("&7... this machine is " + (machine.isComplete() ? "&acomplete" : "&cnot complete")).send(call);
		if (!machine.isComplete()) {
			machine.sendNextStepMessage(sender);
		}
	}

}
