package com.guillaumevdn.gslotmachine.command.machine;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.command.argument.ArgumentString;
import com.guillaumevdn.gcore.lib.object.NeedType;
import com.guillaumevdn.gcore.lib.string.Text;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.PermissionGSM;
import com.guillaumevdn.gslotmachine.TextGSM;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;

public class CmdCreateMachine extends Subcommand {

	private final ArgumentString argId = addArgumentString(NeedType.REQUIRED, false, null, Text.of("id"));

	public CmdCreateMachine() {
		super(true, PermissionGSM.inst().gslotmachineAdmin, TextGSM.commandDescriptionGslotmachineCreateMachine, ConfigGSM.commandsAliasesCreateMachine);
	}

	@Override
	public void perform(CommandCall call) {
		String id = argId.get(call).toLowerCase();
		if (BoardMachines.inst().getCachedValue(id) != null) {
			Text.of("&7Id &c" + id + " &7is already taken.").send(call);
		} else {
			id = id.toLowerCase();

			final Machine machine = new Machine(id);
			BoardMachines.inst().putValue(id, machine);

			Text.of("&7Machine &a" + id + " &7was created.").send(call);
			machine.sendNextStepMessage(call.getSender());
		}
	}

}
