package com.guillaumevdn.gslotmachine.command;

import com.guillaumevdn.gcore.lib.command.CommandCall;
import com.guillaumevdn.gcore.lib.command.Subcommand;
import com.guillaumevdn.gcore.lib.element.editor.EditorGUI;
import com.guillaumevdn.gslotmachine.ConfigGSM;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.PermissionGSM;
import com.guillaumevdn.gslotmachine.TextGSM;

public class CmdEdit extends Subcommand {

	public CmdEdit() {
		super(true, PermissionGSM.inst().gslotmachineEdit, TextGSM.commandDescriptionGslotmachineEdit, ConfigGSM.commandsAliasesEdit);
	}

	@Override
	public void perform(CommandCall call) {
		EditorGUI gui = ConfigGSM.machineTypes.editorGUI(GSlotMachine.inst(), "GSlotMachine", null);
		if (gui != null) {
			gui.openFor(call.getSenderPlayer(), null);
		}
	}

}
