package com.guillaumevdn.gslotmachine.listeners;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.guillaumevdn.gcore.lib.compatibility.material.Mat;
import com.guillaumevdn.gcore.lib.economy.Currency;
import com.guillaumevdn.gcore.lib.string.placeholder.Replacer;
import com.guillaumevdn.gslotmachine.GSlotMachine;
import com.guillaumevdn.gslotmachine.data.machine.BoardMachines;
import com.guillaumevdn.gslotmachine.data.machine.Machine;
import com.guillaumevdn.gslotmachine.lib.machine.active.ActiveMachine;

/**
 * @author GuillaumeVDN
 */
public class PlayerEvents implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void run(PlayerInteractEvent event) {
		if (!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			return;
		}

		// find machine
		final Block block = event.getClickedBlock();
		if (!Mat.fromBlock(block).orAir().getData().getDataName().contains("BUTTON")) {
			return;
		}

		final Location loc = block.getLocation();
		final Machine machine = BoardMachines.inst().streamResultValues(str -> str.filter(m -> loc.equals(m.getButton())).findAny().orElse(null));
		if (machine == null) {
			return;
		}

		// make sure machine is valid
		final Player player = event.getPlayer();
		if (machine.getTypeElement() == null) {
			GSlotMachine.inst().getMainLogger().error(player.getName() + " tried to play with machine " + machine.getId() + " but it has an invalid type '" + machine.getType() + "'");
			return;
		}
		if (!machine.isComplete()) {
			GSlotMachine.inst().getMainLogger().error(player.getName() + " tried to play with machine " + machine.getId() + " but it's not complete");
			return;
		}

		// already active
		if (machine.getActive() != null) {
			return;
		}

		// ensure has enough money and pay
		final Replacer replacer = Replacer.of(player);
		final double cost = machine.getTypeElement().getCost().parse(replacer).orElse(0d);
		if (cost > 0d) {
			final Currency currency = machine.getTypeElement().getCostCurrency().parse(replacer).orNull();
			if (currency == null) {
				return;
			}
			if (!currency.ensureHas(player, cost, true)) {
				return;
			}
			currency.take(player, cost);
		}

		// start machine
		ActiveMachine active = new ActiveMachine(machine, player);
		machine.setActive(active);
		active.start();
	}


}
